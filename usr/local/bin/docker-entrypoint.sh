#!/bin/bash

set -e

function version { echo "$@" | awk -F. '{ printf("%d%03d%03d\n", $1,$2,$3); }'; }

if [ "$1" = 'init' ]; then
    if [ ! -d /var/lib/opendj/data/db ]; then
        echo "Initializing..."
        /usr/local/opendj/setup --cli --propertiesFilePath /etc/opendj/init/setup.props --no-prompt --doNotStart
        mkdir -p /var/lib/opendj/data/config/schema
        cp /etc/opendj/schema/* /var/lib/opendj/data/config/schema/
        /usr/local/opendj/bin/start-ds
        /usr/local/opendj/bin/import-ldif --ldifFile /etc/opendj/init/setup.ldif --backendID userRoot --bindPassword "${LDAP_ROOT_USER_PASS}"
        /usr/local/opendj/bin/stop-ds
        echo "Initialization complete."
    else
        echo "Checking db version..."
        bin_ver=$(version `/usr/local/opendj/bin/dsconfig -V 2> /dev/null`)
        db_ver=$(version `cat /var/lib/opendj/data/config/buildinfo`)
        if [ $bin_ver -gt $db_ver ]; then
            echo "Upgrade required..."
            /usr/local/opendj/upgrade --acceptLicense --no-prompt --force
            echo "Upgrade complete."
        elif [ $bin_ver -lt $db_ver ]; then
            echo "Binary older than db version.  Cannot proceed."
            exit
        else
            echo "Update not required."
        fi
    fi
    echo "Updating schema..."
    cp /etc/opendj/schema/* /var/lib/opendj/data/config/schema/
    
    echo "Updating admin password..."
    encPass=`/usr/local/opendj/bin/encode-password --storageScheme SSHA512 --clearPassword "${LDAP_ROOT_USER_PASS}" | sed 's|.*"\(.*\)".*|\1|'`
    sed -i "s|userPassword:.*|userPassword: ${encPass}|" /var/lib/opendj/data/config/config.ldif

    LDAP_PORT=`grep ldapPort /etc/opendj/init/setup.props | sed -e 's|^ldapPort\s*=\(.*\)|\1|'`
    /usr/local/opendj/bin/start-ds
    /usr/local/opendj/bin/ldapmodify -c -p ${LDAP_PORT} -D "${LDAP_ROOT_USER}" -w ${LDAP_ROOT_USER_PASS} /etc/opendj/init/service_accounts.ldif
    if [ -s /etc/opendj/tls.crt ]; then
      echo "Creating PCKS12 keystore..."
      openssl pkcs12 -export -in /etc/opendj/tls.crt -inkey /etc/opendj/tls.key -out /var/lib/opendj/data/config/keystore.p12 -passout file:/var/lib/opendj/data/config/keystore.pin
      /usr/local/opendj/bin/dsconfig set-key-manager-provider-prop --provider-name "PKCS12" --set enabled:true --hostname localhost --port 4444 --bindDN "cn=Directory Manager" --bindPassword "${LDAP_ROOT_USER_PASS}" --trustAll --no-prompt
    fi
    
    if [ -f /var/lib/opendj/data/config/keystore.p12 ]; then
      echo "Enabling PKCS12 keystore..."
      /usr/local/opendj/bin/dsconfig set-connection-handler-prop --handler-name "LDAPS Connection Handler" --set key-manager-provider:PKCS12 --hostname localhost --port 4444 --bindDN "cn=Directory Manager" --bindPassword "${LDAP_ROOT_USER_PASS}" --trustAll --no-prompt
      /usr/local/opendj/bin/dsconfig set-connection-handler-prop --handler-name "LDAP Connection Handler" --set key-manager-provider:PKCS12 --hostname localhost --port 4444 --bindDN "cn=Directory Manager" --bindPassword "${LDAP_ROOT_USER_PASS}" --trustAll --no-prompt
    else
      echo "Enabling JKS keystore..."
      /usr/local/opendj/bin/dsconfig set-connection-handler-prop --handler-name "LDAPS Connection Handler" --set key-manager-provider:JKS --hostname localhost --port 4444 --bindDN "cn=Directory Manager" --bindPassword "${LDAP_ROOT_USER_PASS}" --trustAll --no-prompt
      /usr/local/opendj/bin/dsconfig set-connection-handler-prop --handler-name "LDAP Connection Handler" --set key-manager-provider:JKS --hostname localhost --port 4444 --bindDN "cn=Directory Manager" --bindPassword "${LDAP_ROOT_USER_PASS}" --trustAll --no-prompt
    fi

    /usr/local/opendj/bin/stop-ds
    exec echo "System ready."
fi

if [ "$1" = 'opendj' ]; then
    exec /usr/local/opendj/bin/start-ds --nodetach
fi

if [ "$1" = 'access-logs' ]; then
    touch /var/lib/opendj/data/logs/ldap-access.audit.json
    exec tail -f /var/lib/opendj/data/logs/ldap-access.audit.json
fi

exec "$@"
