#!/bin/bash

set -e
wget -nv https://github.com/OpenIdentityPlatform/OpenDJ/releases/download/${VERSION}/opendj-${VERSION}.zip
unzip opendj-${VERSION}.zip -x opendj/QuickSetup.app/* opendj/README opendj/Uninstall.app/* opendj/bat/* opendj/bin/ControlPanel.app/* opendj/example-plugin.zip opendj/legal-notices/* opendj/setup.bat opendj/uninstall.bat opendj/upgrade.bat  -d /usr/local
echo "/var/lib/opendj/data" >> /usr/local/opendj/instance.loc