#########################
# Create base container #
#########################
FROM ubuntu:20.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=opendj
ENV APP_GROUP=opendj

# Set version pins
ENV VERSION=4.6.1

# Set base dependencies
ENV BASE_DEPS ca-certificates openjdk-17-jre-headless

# Set build dependencies
ENV BUILD_DEPS wget unzip

# Create app user and group
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -r -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /var/lib/${APP_USER} -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
COPY build.sh /usr/src/build.sh
WORKDIR /usr/src
RUN ./build.sh


##########################
# Create final container #
##########################
FROM base

# Finalize install
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN update-ca-certificates \
 && mkdir /var/lib/opendj \
 && chown -R ${APP_USER} /usr/local/opendj/template \
 && chown -R ${APP_USER} /var/lib/opendj

# Prepare container
EXPOSE 1389/tcp 1636/tcp 4444/tcp 1689/tcp
VOLUME ["/etc/opendj", "/var/lib/opendj"]
USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["opendj"]

